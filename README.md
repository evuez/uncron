# uncron

Convert cron schedules into a human-readable form:

    > uncron "0 6 1 * *"
    At 06:00, on the 1st of every month
    > uncron "0 * 2 1-4 *"
    At the top of every hour, on the 2nd from January to April
    > uncron "10,30,50 * 1 1-6/3 *"
    At minutes 10, 30 and 50 of every hour, on the 1st of every 3 months from January to June

## Building & Running

The recommended solution is to use `nix`:

- Use `nix profile install https://git.k.mulga.net/julien/uncron/snapshot/uncron-main.tar.gz` to install uncron,
- Use `nix run https://git.k.mulga.net/julien/uncron/snapshot/uncron-main.tar.gz` to build and run the binary directly.

If you have `cabal` installed, use `cabal build` or `cabal install`.
