module Cron.Expr (Expr (..), StepLExpr (..), showExpr) where

import Data.List.NonEmpty (NonEmpty, intersperse)
import Data.Semigroup (sconcat)
import Intro

data Expr = Every | Multi (NonEmpty Int) | Range Int Int | Step StepLExpr Int deriving (Show)

data StepLExpr = StepLEvery | StepLRange Int Int deriving (Show)

showExpr :: Expr -> String
showExpr = \case
  Every -> "*"
  Multi ns -> sconcat $ intersperse "," (fmap show ns)
  Range n m -> mconcat [show n, "-", show m]
  Step n m -> mconcat [showStepLExpr n, "/", show m]

showStepLExpr :: StepLExpr -> String
showStepLExpr = \case
  StepLEvery -> "*"
  StepLRange n m -> mconcat [show n, "-", show m]
