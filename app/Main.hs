module Main (main) where

import Control.Arrow (left)
import qualified Cron
import Intro
import System.Environment (getArgs)
import System.IO (getLine)

main :: IO ()
main = do
  cron <- getCron
  putStrLn . showOrErr $ Cron.toProse <$> (Cron.parse =<< cron)

getCron :: IO (Either String String)
getCron = do
  args <- getArgs
  case args of
    [] -> Right <$> getLine
    [x] -> pure $ Right x
    xs -> pure $ Left ("Too many inputs or invalid option: " ++ show xs)

showOrErr :: Either String String -> String
showOrErr = either id id . left ("Error. " ++)
