.PHONY: format
format:
	fourmolu -i app/ src/

.PHONY: lint
lint:
	find {app,src}/ | entr -c hlint src/ app/

.PHONY: run
run:
	find {app,src}/ | entr -cr cabal new-run uncron -- "* * * * *"

.PHONY: build
build:
	find {app,src}/ | entr -c cabal new-build uncron

.PHONY: repl
repl:
	ghcid --allow-eval --lint --command "cabal repl uncron"
